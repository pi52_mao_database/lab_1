alter proc TableRowBrowser as
begin
	if (exists (select * 
						from INFORMATION_SCHEMA.TABLES 
						where TABLE_NAME = 'TableRowCount'))
		begin
			drop table TableRowCount
		end
	create table TableRowCount (TableName VARCHAR(128), RowsCount INT)
	INSERT INTO TableRowCount (TableName, RowsCount)
	EXEC sp_MSforeachtable 'SELECT ''?'' TableName, COUNT(*) RowsCount FROM ?' ;
end

create proc TableColBrowser as
begin
	if (exists (select * 
						from INFORMATION_SCHEMA.TABLES 
						where TABLE_NAME = 'TableColCount'))
		begin
			drop table TableColCount
		end
	create table TableColCount (TableName VARCHAR(128), ColsCount INT)
	insert into TableColCount(TableName, ColsCount) 
		SELECT table_name, count(column_name)
			FROM Library.INFORMATION_SCHEMA.COLUMNS
			group by TABLE_NAME
end

alter proc ColUniqueValuesInTable
	@targetTableName varchar(256) as
begin
	declare culumnsCursor cursor for 
		select COLUMN_NAME from Library.INFORMATION_SCHEMA.COLUMNS
			where TABLE_NAME = @targetTableName
	declare @resultTable as table(columnName varchar(256),  uniqueValuesCount int)
	declare @currentColumn as varchar(256)
	open culumnsCursor
	fetch next from culumnsCursor into @currentColumn
	while @@FETCH_STATUS = 0
	begin
		insert into @resultTable
			EXEC('SELECT columnName = '''+ @currentColumn +''', count(distinct '+ @currentColumn +') FROM ' + @targetTableName)
		fetch next from culumnsCursor into @currentColumn
	end
	close culumnsCursor
	deallocate culumnsCursor
	select * from @resultTable
end


exec TableRowBrowser
select * from TableRowCount

exec TableColBrowser
select * from TableColCount

exec ColUniqueValuesInTable 'Books'

alter trigger GetExpelledReaderBooks on Readers for update as
begin
	
end

update Readers set homeAddress = '������� 33' where homeAddress = '������� 1'
select * from Readers